import React from 'react';
import { Root } from "native-base";
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screen/HomeScreen';
import AuthScreen from './screen/AuthScreen';
import WelcomeScreen from './screen/WelcomeScreen';
import LoginScreen from './screen/LoginScreen';
import LoginNextScreen from './screen/LoginNextScreen';
import SignupScreen from './screen/SignupScreen';
import SearchScreen from './screen/SearchScreen';
import UserScreen from './screen/UserScreen';
import AddFriendScreen from './screen/AddFriendScreen';
import NewFriendScreen from './screen/NewFriendScreen';
import PersonChatScreen from './screen/PersonChatScreen';
import './database/sqlite';
import SettingScreen from './screen/SettingScreen';

const AppNavigator = createStackNavigator({
  Welcome: {
    screen: WelcomeScreen,
  },
  Home: {
    screen: HomeScreen,
  },
  Auth: {
    screen: AuthScreen,
  },
  Login: {
    screen: LoginScreen,
  },
  LoginNext: {
    screen: LoginNextScreen,
  },
  Signup: {
    screen: SignupScreen,
  },
  Search: {
    screen: SearchScreen,
  },
  User: {
    screen: UserScreen,
  },
  AddFriend: {
    screen: AddFriendScreen,
  },
  NewFriend: {
    screen: NewFriendScreen,
  },
  PersonChat: {
    screen: PersonChatScreen,
  },
  Setting: {
    screen: SettingScreen,
  },
}, {
    initialRouteName: 'Welcome',
  });

class App extends React.Component {
  render() {
    return (<Root><AppNavigator /></Root>);
  }
}
export default App;
