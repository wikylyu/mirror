import { DB } from './sqlite';
import { MessageType } from '../api/ws';


export const InsertMessage = (id, version, status, fromUserId, toUserId, toGroupId, type, contentMessage) => {
    const db = DB();

    fromUserId = fromUserId || '';
    toUserId = toUserId || '';
    toGroupId = toGroupId || '';
    contentMessage = JSON.stringify(contentMessage);
    return db.executeSql(`INSERT OR IGNORE INTO message(id, version, status, fromUserId, toUserId, toGroupId, type, contentMessage) VALUES(?,?,?,?,?,?,?,?)`, [id, version, status, fromUserId, toUserId, toGroupId, type, contentMessage]).then(r => {
        console.log('message inserted');
    });
};

export const UpdateMessageStatus = (id, status) => {
    const db = DB();

    return db.executeSql(`UPDATE message SET status=? WHERE id=?`, [status, id]);
};

export const FindPersonMessagesByUserId = (userId) => {
    const db = DB();
    userId = String(userId);

    return db.executeSql(`SELECT * FROM message WHERE type = ? AND (fromUserId=? OR toUserId=?) ORDER BY ctime`, [MessageType.ContentMessage, userId, userId]).then((rr) => {
        let results = [];
        const r = rr[0];
        for (let i = 0; i < r.rows.length; i++) {
            let row = r.rows.item(i);
            row.contentMessage = JSON.parse(row.contentMessage);
            results.push(row);
        }
        return results;
    });
};