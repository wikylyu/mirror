import React from 'react';
import { Text, ActivityIndicator } from 'react-native';
import { View, Thumbnail, Icon } from 'native-base';


export class LeftChatItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { item } = this.props;
        return (
            <View style={{ display: 'flex', flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                <Thumbnail style={{ width: 40, height: 40 }} square source={{ uri: item.avatar }}></Thumbnail>
                <Text style={{ marginLeft: 10, backgroundColor: 'white', padding: 6, borderRadius: 4 }}>{item.contentMessage.content.text}</Text>
                <View style={{ marginLeft: 10 }}>
                    {item.status === 0 ? <ActivityIndicator size="small" color="#03A9F4" /> : null}
                    {item.status === 2 ? <Icon name="md-close-circle" style={{ fontSize: 18, color: '#F44336' }} /> : null}
                </View>
            </View>
        );
    }
};

export class RightChatItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { item } = this.props;
        return (
            <View style={{ display: 'flex', flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <View style={{ marginRight: 10 }}>
                    {item.status === 0 ? <ActivityIndicator size="small" color="#03A9F4" /> : null}
                    {item.status === 2 ? <Icon name="md-close-circle" style={{ fontSize: 18, color: '#F44336' }} /> : null}
                </View>
                <Text style={{ marginRight: 10, backgroundColor: 'white', padding: 6, borderRadius: 4 }}>{item.contentMessage.content.text}</Text>
                <Thumbnail style={{ width: 40, height: 40 }} square source={{ uri: item.avatar }}></Thumbnail>
            </View>
        );
    }
};
