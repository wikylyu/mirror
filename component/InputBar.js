import React from 'react';
import { View, Text } from 'react-native';
import { Input, Button, Icon } from 'native-base';

class InputBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
        };
    }

    render() {
        return (
            <View style={{ height: 48, width: '100%', flexDirection: 'row', backgroundColor: 'white', display: 'flex', paddingLeft: 10, paddingRight: 10, justifyContent: 'center' }}>
                <Input style={{ borderBottomColor: '#04be01', borderBottomWidth: 1, marginRight: 10, fontSize: 14 }} value={this.state.text} onChangeText={text => this.setState({ text })} placeholder="" onSubmitEditing={this.onSubmit}></Input>
                <View style={{ height: '100%', justifyContent: 'center' }}>
                    <Button onPress={this.onSubmit} success style={{ width: 52, height: 36, justifyContent: 'center' }}><Text style={{ color: 'white' }}>发送</Text></Button>
                </View>
            </View >
        );
    }

    onSubmit = () => {
        if (!this.state.text) {
            return;
        }
        this.props.onSubmit(this.state.text);
        this.setState({ text: '' });
    }
}

export default InputBar;