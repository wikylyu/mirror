import React from 'react';
import { View, Text, StatusBar, StyleSheet } from 'react-native';
import { Button, Icon, Thumbnail, Row, Col } from 'native-base';
import { GetUser } from '../api/account';
import { GetFriendById } from '../database/friend';
import { StackActions, NavigationActions } from 'react-navigation';


class UserScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: '详细资料',
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTintColor: '#fff',
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            isFriend: false,
            user: this.props.navigation.getParam('user', {}),
        };
        this.unmounted = false;
        GetUser(this.state.user.id).then((data) => {
            if (this.unmounted) {
                return;
            }
            this.setState({ user: data });
        });
        GetFriendById(this.state.user.id).then((f) => {
            if (this.unmounted) {
                return;
            }
            this.setState({ isFriend: f });
        });
    }
    render() {
        const user = this.state.user || {};
        var button;
        if (this.state.isFriend) {
            button = (<Button full style={styles.button} onPress={this.onSendMessage}><Text style={{ color: 'white' }}>发消息</Text></Button>);
        } else {
            button = (<Button full style={styles.button} onPress={this.onAddFriend}><Text style={{ color: 'white' }}>添加到通信录</Text></Button>);
        }

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#222" barStyle="light-content" />
                <Row style={styles.row}>
                    <Col style={styles.rowLeft}>
                        <Thumbnail square source={{ uri: user.avatar }} />
                    </Col>
                    <Col style={styles.rowRight}>
                        <Text style={styles.textNickname}>{user.nickname} </Text>
                        <Text style={styles.textPhone}>手机号：{user.phone} </Text>
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.rowLeft}>
                        <Text>地区</Text>
                    </Col>
                    <Col style={styles.rowRight}>
                        <Text>浙江 永康</Text>
                    </Col>
                </Row>
                <Row style={styles.rowDivider}>
                    <View style={styles.divider}></View>
                </Row>
                <Row style={[styles.row, styles.rowFollow]}>
                    <Col style={styles.rowLeft}>
                        <Text>个人相册</Text>
                    </Col>
                    <Col style={[styles.rowRight, styles.colThumbnail]}>
                        <Thumbnail style={styles.thumbnail} square source={{ uri: user.avatar }} />
                        <Thumbnail style={styles.thumbnail} square source={{ uri: user.avatar }} />
                        <Thumbnail style={styles.thumbnail} square source={{ uri: user.avatar }} />
                    </Col>
                </Row>
                <Row style={styles.rowDivider}>
                    <View style={styles.divider}></View>
                </Row>
                <Row style={[styles.row, styles.rowFollow]}>
                    <Text>更多</Text>
                </Row>
                {button}
            </View>
        );
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    onAddFriend = () => {
        this.props.navigation.navigate({ routeName: 'AddFriend', params: { user: this.state.user } });
    }

    onSendMessage = () => {
        this.props.navigation.navigate({ routeName: 'PersonChat', params: { user: this.state.user } });
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: '100%',
        display: 'flex',
    },
    divider: {
        width: '100%',
        height: 1,
        backgroundColor: '#EEE',
    },
    row: {
        backgroundColor: 'white',
        marginTop: 18,
        padding: 12,
        flex: 0,
        flexDirection: 'row',
    },
    rowDivider: {
        backgroundColor: 'white',
        flex: 0,
        flexDirection: 'row',
    },
    rowFollow: {
        marginTop: 0,
    },
    rowLeft: {
        width: 75,
        justifyContent: 'center',
    },
    rowRight: {
        // marginLeft: 20,
    },
    colThumbnail: {
        flexDirection: 'row',
    },
    thumbnail: {
        marginRight: 8,
    },
    textNickname: {
        fontSize: 16,
        fontWeight: 'normal',
    },
    textPhone: {
        fontSize: 13,
        fontWeight: 'normal',
        color: 'gray',
    },
    button: {
        marginTop: 20,
        backgroundColor: '#04be01',
        marginLeft: 12,
        marginRight: 12,
    },
});


export default UserScreen;