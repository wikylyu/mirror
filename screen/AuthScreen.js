import React from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';
import { Container, Button, Text } from 'native-base';

class AuthScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <ImageBackground source={require('../asset/img/startup.jpeg')} style={styles.background}>
        <View style={styles.buttonBox}>
          <Button block success style={[styles.button, styles.buttonLogin]} onPress={this.onLogin} >
            <Text style={styles.buttonText}>登录</Text>
          </Button>
          <Button block success style={[styles.button, styles.buttonSignup]} >
            <Text style={[styles.buttonText, styles.buttonTextSignup]} onPress={this.onSignup}>注册</Text>
          </Button>
        </View>

      </ImageBackground>
    );
  }

  onLogin = () => {
    this.props.navigation.navigate({ routeName: 'Login' });
  }

  onSignup = () => {
    this.props.navigation.navigate({ routeName: 'Signup' });
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
  },
  buttonBox: {
    width: '100%',
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 20,
    flexDirection: 'row',
  },
  button: {
    height: 50,
    width: '40%',
  },
  buttonLogin: {
    backgroundColor: '#04be01',
  },
  buttonSignup: {
    backgroundColor: '#ffff',
    marginLeft: 'auto',
  },
  buttonText: {
    fontSize: 20,
    width: '100%',
    lineHeight: 50,
    textAlign: 'center',
  },
  buttonTextSignup: {
    color: 'black',
  }
})

export default AuthScreen
