import React from 'react';
import { Button, Icon, Input } from 'native-base';
import { View, AsyncStorage, StatusBar, ScrollView } from 'react-native';
import InputBar from '../component/InputBar';
import { SendMessage, ContentMessage, ContentMessageType, RegisterOnMessage, CancelOnMessage, MessageType, MessageAck, NotificationType } from '../api/ws';
import { FindPersonMessagesByUserId, InsertMessage, UpdateMessageStatus } from '../database/message';
import { LeftChatItem, RightChatItem } from '../component/ChatItem';
import { InsertSession, SessionType, UpdateSessionUnread, GetSessionUnread } from '../database/session';
import { GetUser, GetAccount } from '../api/account';
import { GetFriendRequest } from '../api/friend';
import { InsertFriendRequest } from '../database/friend';
import { PushMessage } from '../push';


class PersonChatScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: params.user.nickname,
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTintColor: '#fff',
            headerRight: (
                <View><Button transparent onPress={params.onAdd}><Icon name="md-menu" style={{ color: 'white' }}></Icon></Button></View>
            ),
        };
    }

    constructor(props) {
        super(props);
        this.user = this.props.navigation.getParam('user', {});
        this.self = {};

        this.state = {
            messages: []
        };

        GetAccount().then(a => {
            this.self = a;
            FindPersonMessagesByUserId(this.user.id).then((messages) => {
                for (let i = 0; i < messages.length; i++) {
                    if (messages[i].fromUserId === String(this.user.id)) {
                        messages[i].avatar = this.user.avatar;
                    } else {
                        messages[i].avatar = a.avatar;
                    }
                }
                this.setState({ messages });
            });
        });

        RegisterOnMessage(this.onmessage);
    }

    render() {
        const { messages } = this.state;
        return (
            <View style={{ flexDirection: 'column', display: 'flex', flex: 1 }}>
                <StatusBar backgroundColor="#222" barStyle="light-content" />
                <ScrollView ref={ref => this.scrollView = ref} alwaysBounceVertical="true" onContentSizeChange={this.onScrollViewContentSizeChange} contentContainerStyle={{ paddingTop: 5, paddingBottom: 5 }}>
                    {messages.map((m, k) => (
                        <View key={k} style={{ marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10 }}>
                            {m.toUserId !== String(this.user.id) ? <LeftChatItem item={m}></LeftChatItem> : <RightChatItem item={m}> </RightChatItem>}
                        </View>
                    ))}
                </ScrollView>
                <InputBar onSubmit={this.onSubmit}></InputBar>
            </View >
        );
    }

    componentDidMount() {
        this.scrollView.scrollToEnd({ animated: false });
        UpdateSessionUnread(SessionType.Person, this.user.id, 0);
    }

    componentWillUnmount() {
        CancelOnMessage(this.onmessage);
    }

    onScrollViewContentSizeChange = (w, h) => {
        console.log(w, h);
        this.scrollView.scrollToEnd();
    }

    onSubmit = (text) => {
        const m = ContentMessage(this.user.id, ContentMessageType.Text, { text: text });
        let status = 0;
        if (!SendMessage(m)) {
            status = 2;
        }
        m.status = status;
        m.avatar = this.self.avatar;
        const { messages } = this.state;
        messages.push(m);
        this.setState({ messages });

        InsertMessage(m.id, m.version, status, m.fromUserId, m.toUserId, m.toGroupId, m.type, m.contentMessage);
        InsertSession(SessionType.Person, this.user.id, m.contentMessage.content.text, this.user.nickname, this.user.avatar, 0);
    }


    onmessage = (m) => {
        if (m.type === MessageType.Notification && m.notification) {
            this.onNotification(m);
        } else if (m.type === MessageType.ContentMessage && m.contentMessage) {
            this.onContentMessage(m);
        } else if (m.type === MessageType.MessageAck && m.messageAck) {
            this.onMessageAck(m);
        }
    }

    onNotification = (m) => {
        if (m.notification.type === NotificationType.FriendRequest) {
            const frid = m.notification.content.id;
            GetFriendRequest(frid).then((body) => {
                const data = body.data;
                if (!data) {
                    return;
                }
                InsertFriendRequest(data.id, data.user_id, data.avatar, data.nickname, data.message, data.ctime);
                AsyncStorage.getItem('newFriend').then(nf => {
                    nf = parseInt(nf) || 0;
                    nf += 1;
                    AsyncStorage.setItem('newFriend', String(nf));
                });
            });
        }

        if (m.notification.needAck) {
            SendMessage(MessageAck(m.id));
        }
    }

    onMessageAck = (m) => {
        const mid = m.messageAck.msgId;
        UpdateMessageStatus(mid, 1).then(() => {
            const messages = this.state.messages.slice();
            for (let i = 0; i < messages.length; i++) {
                if (messages[i].id === mid) {
                    messages[i].status = 1;
                    break;
                }
            }
            this.setState({ messages });
        });
    }

    onContentMessage = (m) => {
        InsertMessage(m.id, m.version, 1, m.fromUserId, m.toUserId, m.toGroupId, m.type, m.contentMessage);

        if (m.fromUserId === String(this.user.id)) {
            const nm = Object.assign(m, { avatar: this.user.avatar });
            const { messages } = this.state;
            messages.push(nm);
            this.setState({ messages });

            PushMessage(m.fromUserId, this.user.nickname, m.contentMessage.content.text);
            InsertSession(SessionType.Person, m.fromUserId, m.contentMessage.content.text, this.user.nickname, this.user.avatar, 0).then(() => {
                GetSessionUnread(SessionType.Person, m.fromUserId).then(unread => {
                    let message = m.contentMessage.content.text;
                    if (unread > 1) {
                        message = '[' + unread + '条]' + message;
                    }
                    PushMessage(m.fromUserId, this.user.nickname, message);
                });
            });
        } else {
            GetUser(m.fromUserId).then((data) => {
                if (!data) {
                    return;
                }

                InsertSession(SessionType.Person, m.fromUserId, m.contentMessage.content.text, data.nickname, data.avatar, 1).then(() => {
                    GetSessionUnread(SessionType.Person, m.fromUserId).then(unread => {
                        let message = m.contentMessage.content.text;
                        if (unread > 1) {
                            message = '[' + unread + '条]' + message;
                        }
                        PushMessage(m.fromUserId, data.nickname, message);
                    });
                });
            });
        }
        SendMessage(MessageAck(m.id));
    }
};


export default PersonChatScreen;