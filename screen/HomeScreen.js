import React from 'react';
import { StatusBar, StyleSheet, View, AsyncStorage } from 'react-native';
import { MessageType, NotificationType, MessageAck, CloseWebSocket, CancelOnMessage, ReconnectWebSocket, RegisterOnMessage, SendMessage } from '../api/ws';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Icon, Button, Toast, Row, Col, Badge } from 'native-base';
import TabNavigator from 'react-native-tab-navigator';
import { GetFriendRequest, FetchUserFriends } from '../api/friend';
import { InsertFriendRequest, UpdateFriends, FindFriends, GetFriendById } from '../database/friend';
import { NavigationEvents } from 'react-navigation';
import { InsertMessage, UpdateMessageStatus } from '../database/message';
import { InsertSession, SessionType, FindSessions, GetSessionUnread } from '../database/session';
import { GetUser, GetAccount } from '../api/account';
import { PushMessage } from '../push';

class HomeScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: '镜像',
            headerStyle: {
                backgroundColor: '#222',
            },
            headerTitleStyle: {
                color: 'white',
                fontWeight: 'normal',
            },
            headerRight: (
                <View style={{ flexDirection: 'row' }}>
                    <Button transparent onPress={params.onSearch}><Icon name="search" style={{ color: 'white' }}></Icon></Button>
                    <Button transparent onPress={params.onAdd}><Icon name="add" style={{ color: 'white' }}></Icon></Button>
                </View>
            ),
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'chat',
            sessions: [],
            friends: [],
            newFriend: 0,
            account: {},
        };

        ReconnectWebSocket();
        RegisterOnMessage(this.onmessage);

    }
    componentDidMount() {
        this.props.navigation.setParams({ onSearch: this.onSearch, onAdd: this.onAdd });
    }
    componentWillUnmount() {
        CloseWebSocket();
        CancelOnMessage(this.onmessage);
    }
    render() {
        const { account, sessions } = this.state;
        for (let i = 0; i < sessions.length; i++) {
            sessions[i].time = new Date(sessions[i].utime.replace(' ', 'T'));
        }
        return (
            <View style={styles.container}>
                <NavigationEvents
                    onWillFocus={this.onWillFocus}
                    onDidFocus={this.onDidFocus}
                    onWillBlur={this.onWillBlur}></NavigationEvents>
                <StatusBar backgroundColor="#222" />
                <TabNavigator>
                    <TabNavigator.Item
                        style={styles.item}
                        selected={this.state.selectedTab === 'chat'}
                        title="镜像"
                        selectedTitleStyle={styles.tabSelected}
                        renderIcon={() => <Icon name="ios-chatbubbles-outline" />}
                        renderSelectedIcon={() => <Icon name="ios-chatbubbles" style={styles.tabSelected} />}
                        onPress={() => this.setState({ selectedTab: 'chat' })}>
                        <List avatar style={{ backgroundColor: 'white', paddingRight: 18 }} dataArray={sessions}
                            renderRow={(item) =>
                                <ListItem badge style={{ borderBottomColor: '#EEE', borderBottomWidth: 1 }} thumbnail onPress={() => this.onSession(item)}>
                                    <Left>
                                        <Thumbnail style={{ width: 50, height: 50 }} square source={{ uri: item.avatar }} />
                                        {item.unread > 0 ? <Badge style={{ position: 'absolute', width: 24, height: 24, right: -12, top: -4 }}><Text style={{ fontSize: 12 }}>{item.unread}</Text></Badge> : null}
                                    </Left>
                                    <Body style={{ borderBottomWidth: 0 }}>
                                        <Text>{item.name}</Text>
                                        <Text note numberOfLines={1} style={{ fontSize: 14 }}>{item.message}</Text>
                                    </Body>
                                    <Right>
                                        <Text note style={{ fontSize: 14 }}>{item.time.getHours() + ':' + item.time.getMinutes()}</Text>
                                    </Right>
                                </ListItem>
                            }>
                        </List>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        style={styles.item}
                        selected={this.state.selectedTab === 'contacts'}
                        title="通讯录"
                        selectedTitleStyle={styles.tabSelected}
                        renderIcon={() => <Icon name="ios-people-outline" />}
                        renderSelectedIcon={() => <Icon name="ios-people" style={styles.tabSelected} />}
                        onPress={() => this.setState({ selectedTab: 'contacts' })}>
                        <View style={styles.content}>
                            <View style={styles.itemGroup}>
                                <ListItem icon onPress={this.onNewFriend} style={[styles.listItem, { borderBottomWidth: 1, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-person-add" active style={[styles.iconLeft, { backgroundColor: '#fa9e3b' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text>新的朋友</Text>
                                    </Body>
                                    {this.state.newFriend > 0 ? (
                                        <Right>
                                            <Badge style={{ width: 24, height: 24, }}><Text style={{ fontSize: 12 }}>1</Text></Badge>
                                        </Right>) : null}
                                </ListItem>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-people" active style={[styles.iconLeft, { backgroundColor: '#04be01' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text>群聊</Text>
                                    </Body>
                                </ListItem>
                            </View>

                            <List style={{ backgroundColor: 'white', marginTop: 16, paddingRight: 18 }} dataArray={this.state.friends}
                                renderRow={(item) =>
                                    <ListItem style={{ borderBottomWidth: 1, borderBottomColor: '#EEE' }} thumbnail onPress={() => this.onPersonChat(item)}>
                                        <Left>
                                            <Thumbnail square style={styles.avatar} source={{ uri: item.avatar }} />
                                        </Left>
                                        <Body style={{ borderBottomWidth: 0 }}>
                                            <Text>{item.nickname}</Text>
                                        </Body>
                                    </ListItem>
                                }>
                            </List>
                        </View>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        style={styles.item}
                        selected={this.state.selectedTab === 'explore'}
                        title="发现"
                        selectedTitleStyle={styles.tabSelected}
                        renderIcon={() => <Icon name="ios-compass-outline" />}
                        renderSelectedIcon={() => <Icon name="ios-compass" style={styles.tabSelected} />}
                        onPress={() => this.setState({ selectedTab: 'explore' })}>
                        <View style={styles.content}>
                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-aperture" style={[styles.iconLeft, { color: '#f25771' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>朋友圈</Text>
                                    </Body>
                                </ListItem>
                            </View>
                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-qr-scanner" style={[styles.iconLeft, { color: 'blue' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>扫一扫</Text>
                                    </Body>
                                </ListItem>
                            </View>
                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-search" style={[styles.iconLeft, { color: '#FF9800' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>搜一搜</Text>
                                    </Body>
                                </ListItem>
                            </View>

                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 1 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-pricetags" style={[styles.iconLeft, { color: '#F4511E' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>购物</Text>
                                    </Body>
                                </ListItem>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-game-controller-b" style={[styles.iconLeft, { color: '#4CAF50' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>游戏</Text>
                                    </Body>
                                </ListItem>
                            </View>

                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0 }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-infinite" style={[styles.iconLeft, { color: '#673AB7' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>小程序</Text>
                                    </Body>
                                </ListItem>
                            </View>
                        </View>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        style={styles.item}
                        selected={this.state.selectedTab === 'me'}
                        title="我"
                        selectedTitleStyle={styles.tabSelected}
                        renderIcon={() => <Icon name="ios-person-outline" />}
                        renderSelectedIcon={() => <Icon name="ios-person" style={styles.tabSelected} />}
                        onPress={() => this.setState({ selectedTab: 'me' })}>
                        <View style={styles.content}>
                            <List avatar style={{ backgroundColor: 'white', marginTop: 20 }} >
                                <ListItem badge thumbnail style={{ marginTop: 5, marginBottom: 5, borderBottomWidth: 0, borderBottomColor: '#EEE' }}>
                                    <Left>
                                        <Thumbnail style={{ width: 64, height: 64 }} square source={{ uri: account.avatar }} />
                                    </Left>
                                    <Body style={{ borderBottomWidth: 0 }}>
                                        <Text style={{ fontSize: 20 }}>{account.nickname}</Text>
                                        <Text note numberOfLines={1} style={{ fontSize: 14 }}>手机号：{account.phone}</Text>
                                    </Body>
                                </ListItem>
                            </List>
                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="ios-card" style={[styles.iconLeft, { color: '#6200EE' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>钱包</Text>
                                    </Body>
                                </ListItem>
                            </View>

                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 1, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="logo-facebook" style={[styles.iconLeft, { color: '#5f95fc' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>收藏</Text>
                                    </Body>
                                </ListItem>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 1, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-images" style={[styles.iconLeft, { color: '#1E88E5' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>相册</Text>
                                    </Body>
                                </ListItem>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 1, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="ios-folder-open" style={[styles.iconLeft, { color: '#1E88E5' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>卡包</Text>
                                    </Body>
                                </ListItem>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0, borderBottomColor: '#EEE' }]}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-happy" style={[styles.iconLeft, { color: '#CDDC39' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>表情</Text>
                                    </Body>
                                </ListItem>
                            </View>

                            <View style={[styles.itemGroup, { marginTop: 20 }]}>
                                <ListItem icon style={[styles.listItem, { borderBottomWidth: 0, borderBottomColor: '#EEE' }]} onPress={this.onSetting}>
                                    <Left style={{ flex: 0 }}>
                                        <Icon name="md-settings" style={[styles.iconLeft, { color: '#1E88E5' }]}></Icon>
                                    </Left>
                                    <Body style={{ paddingLeft: 4, borderBottomWidth: 0 }}>
                                        <Text style={styles.textRight}>设置</Text>
                                    </Body>
                                </ListItem>
                            </View>
                        </View>
                    </TabNavigator.Item>
                </TabNavigator>
            </View>
        );
    }

    onWillFocus = () => {
        this.fetchUserFriends();
        this.findUserSessions();
        this.findNewFriend();
        this.findAccount();
    }

    onDidFocus = () => {
        this.findUserSessions();
    }

    onWillBlur = () => {
        console.log('will blur');
    }

    onmessage = (m) => {
        if (m.type === MessageType.Notification && m.notification) {
            this.onNotification(m);
        } else if (m.type === MessageType.ContentMessage && m.contentMessage) {
            this.onContentMessage(m);
        } else if (m.type === MessageType.MessageAck && m.messageAck) {
            this.onMessageAck(m);
        }
    }

    onNotification = (m) => {
        if (m.notification.type === NotificationType.FriendRequest) {
            const frid = m.notification.content.id;
            GetFriendRequest(frid).then((body) => {
                const data = body.data;
                if (!data) {
                    return;
                }
                InsertFriendRequest(data.id, data.user_id, data.avatar, data.nickname, data.message, data.ctime);
                AsyncStorage.getItem('newFriend').then(nf => {
                    nf = parseInt(nf) || 0;
                    nf += 1;
                    AsyncStorage.setItem('newFriend', String(nf));
                    this.setState({ newFriend: nf });
                });
            });
        }

        if (m.notification.needAck) {
            SendMessage(MessageAck(m.id));
        }
    }

    onMessageAck = (m) => {
        UpdateMessageStatus(m.messageAck.msgId, 1).then(() => { });
    }

    onContentMessage = (m) => {
        InsertMessage(m.id, m.version, 1, m.fromUserId, m.toUserId, m.toGroupId, m.type, m.contentMessage);
        GetUser(m.fromUserId).then((data) => {
            if (!data) {
                return;
            }
            InsertSession(SessionType.Person, m.fromUserId, m.contentMessage.content.text, data.nickname, data.avatar, 1).then(() => {
                this.findUserSessions();
                GetSessionUnread(SessionType.Person, m.fromUserId).then(unread => {
                    let message = m.contentMessage.content.text;
                    if (unread > 1) {
                        message = '[' + unread + '条]' + message;
                    }
                    PushMessage(m.fromUserId, data.nickname, message);
                });
            });
        });
        SendMessage(MessageAck(m.id));
    }

    findAccount = () => {
        GetAccount().then(a => {
            this.setState({ account: a || {} });
        });
    }

    findNewFriend = () => {
        AsyncStorage.getItem('newFriend').then(nf => {
            nf = parseInt(nf) || 0;
            this.setState({ newFriend: nf });
        });
    }

    findUserFriends = () => {
        FindFriends().then((friends) => {
            console.log('FindFriends', friends);
            this.setState({ friends });
        });
    }

    fetchUserFriends = () => {
        FetchUserFriends().then((body) => {
            if (body.status != 0) {
                throw 'Error';
            }
            const friends = body.data || [];
            console.log('friends', friends);
            UpdateFriends(friends).then(() => this.findUserFriends());
        }).catch((e) => {
            this.findUserFriends();
        });
    }

    findUserSessions = () => {
        FindSessions().then(sessions => {
            this.setState({ sessions });
        });
    }

    onSearch = () => {
        this.props.navigation.navigate({ routeName: 'Search' });
    }
    onAdd = () => {
    }

    onSetting = () => {
        this.props.navigation.navigate({ routeName: 'Setting' });
    }
    onNewFriend = () => {
        this.props.navigation.navigate({ routeName: 'NewFriend' });
    }
    onPersonChat = (item) => {
        this.props.navigation.navigate({ routeName: 'PersonChat', params: { user: item } });
    }
    onSession = (item) => {
        GetFriendById(item.targetId).then(f => {
            if (!f) {
                return;
            }
            this.props.navigation.navigate({ routeName: 'PersonChat', params: { user: f } });
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: '100%',
    },
    tabSelected: {
        color: '#04be01',
    },
    item: {
        width: '100%',
        height: '100%',
    },
    content: {
        flexDirection: 'column',
        height: '100%',
    },
    itemGroup: {
        paddingLeft: 18,
        paddingRight: 18,
        flex: 0,
        backgroundColor: 'white',
    },
    listItem: {
        marginLeft: 0,
        height: 50,
        borderBottomColor: '#EEE',
    },
    avatar: {
        width: 38,
        height: 38,
    },
    iconLeft: {
        color: 'white',
        width: 38,
        height: 38,
        lineHeight: 38,
        textAlign: 'center',
        fontSize: 26,
    },
    textRight: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
    }
});

export default HomeScreen;
