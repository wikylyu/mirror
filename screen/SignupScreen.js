import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Content, Button, Icon, Text, Input, Toast } from 'native-base';
import { Col, Row } from 'react-native-easy-grid';
import ImagePicker from 'react-native-image-crop-picker';
import { UploadImage, GetImageURL } from '../api/file';
import { Signup } from '../api/account';
import { StackActions, NavigationActions } from 'react-navigation';

class SignupScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: '#00000000',
            },
            headerLeft: (<Button transparent onPress={() => { navigation.goBack(); }}><Icon name='arrow-back' /></Button>),
        };
    };
    constructor(props) {
        super(props);
        this.state = { phone: '', password: '', nickname: '', avatar: null };
        this.loading = false;
    }
    render() {
        return (
            <Content style={styles.content}>
                <Row style={styles.row}>
                    <Text style={styles.titleText}>手机号注册</Text>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>昵称</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.text, styles.phoneInput]} placeholder='例如：陈晨' value={this.state.nickname} onChangeText={(nickname) => this.setState({ nickname })}></Input></Col>
                    <Col style={styles.colImage}><TouchableOpacity onPress={this.onImage}><Image style={styles.image} source={{ uri: this.state.avatar }} ></Image></TouchableOpacity></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>国家/地区</Text></Col>
                    <Col style={[styles.colRight, styles.colRegion]}><Text style={[styles.text, styles.textRegion]}>中国 ( +86 )</Text></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>手机号</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.text, styles.phoneInput]} keyboardType="numeric" placeholder='请填写手机号' value={this.state.phone} onChangeText={(phone) => this.setState({ phone })}></Input></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>密码</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.text, styles.phoneInput]} placeholder='请填写密码' secureTextEntry={true} value={this.state.password} onChangeText={(password) => this.setState({ password })}></Input></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row styles={styles.row}>
                    <Button block style={styles.buttonNext} onPress={this.onSignup}><Text style={styles.textNext}>注册</Text></Button>
                </Row>
            </Content >
        );
    }

    onSignup = () => {
        console.log(this.state);

        const { phone, password, nickname, avatar } = this.state;
        if (phone.length != 11 || !password || !nickname || !avatar || this.loading) {
            return;
        }

        this.loading = true;

        Signup(phone, password, nickname, avatar).then((body) => {
            this.loading = false;
            if (body.status === 10004) {
                Toast.show({
                    text: '手机号已经注册，请直接登录',
                    duration: 4000,
                });
                return;
            } else if (body.status !== 0) {
                throw 'Error';
            } else {
                AsyncStorage.setItem('token', String(body.data.id)).then(() => {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Home' })],
                    });

                    this.props.navigation.dispatch(resetAction);
                });
                AsyncStorage.setItem('userId', String(body.data.user_id)).then(() => { });
            }
        }).catch((err) => {
            console.log(err);
            this.loading = false;
            Toast.show({
                text: '注册失败',
                duration: 4000,
            });
        });
    }

    onImage = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true
        }).then(image => {
            UploadImage(image.path, image.mime).then(body => {
                console.log(body);
                if (body.status !== 0) {
                    throw 'Error';
                }
                const url = GetImageURL(body.data.fileid);
                this.setState({ avatar: url });
            }).catch(() => {
                Toast.show({
                    text: '上传失败',
                    duration: 4000,
                });
            });
        }).catch();
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    text: {
        fontSize: 14,
        color: '#333',
    },
    row: {
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'flex-end',
    },
    titleText: {
        fontSize: 26,
        paddingTop: 18,
        paddingBottom: 18,
    },
    colLeft: {
        height: 50,
        width: 100,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    colRight: {
        height: 50,
        flex: 1,
    },
    colRegion: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    colImage: {
        right: 0,
        marginLeft: 'auto',
        flex: 0,
    },
    image: {
        width: 64,
        height: 64,
        backgroundColor: '#999',
        borderRadius: 3,
    },
    textRegion: {
        color: '#04be01',
    },
    divider: {
        backgroundColor: '#CCC',
        width: '100%',
        height: 1,
    },
    phoneInput: {
        paddingLeft: 0,
    },
    rowOther: {
        marginTop: 22,
        marginBottom: 22,
    },
    textOther: {
        color: '#00506c',
    },
    buttonNext: {
        width: '100%',
        height: 50,
        marginTop: 30,
        backgroundColor: '#04be01',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textNext: {
        fontSize: 18,
    },
})


export default SignupScreen;