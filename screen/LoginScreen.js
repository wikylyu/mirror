import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Content, Button, Icon, Text, Input } from 'native-base';
import { Col, Row } from 'react-native-easy-grid';

class LoginScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: '#00000000',
            },
            headerLeft: (<Button transparent onPress={() => { navigation.goBack(); }}><Icon name='close' /></Button>),
        };
    };
    constructor(props) {
        super(props);
        this.state = { phone: '' };
    }
    render() {
        return (
            <Content style={styles.content}>
                <Row style={styles.row}>
                    <Text style={[styles.titleText]}>手机号登录</Text>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>国家/地区</Text></Col>
                    <Col style={[styles.colRight, styles.colRegion]}><Text style={[styles.text, styles.textRegion]}>中国 ( +86 )</Text></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>手机号</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.phoneInput, styles.text]} keyboardType="numeric" placeholder='请填写手机号' value={this.state.phone} onChangeText={(phone) => this.setState({ phone })}></Input></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={[styles.row, styles.rowOther]}>
                    <Text style={[styles.textOther, styles.text]}>用镜像号/邮箱登录</Text>
                </Row>
                <Row styles={styles.row}>
                    <Button block style={styles.buttonNext} onPress={this.onNext}><Text style={styles.textNext}>下一步</Text></Button>
                </Row>
            </Content >
        );
    }

    onNext = () => {
        const { phone } = this.state;
        if (!phone || phone.length != 11) {
            return;
        }
        this.props.navigation.navigate({ routeName: 'LoginNext', params: { country_code: '86', phone: phone } });
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    text: {
        fontSize: 14,
        color: '#333',
    },
    row: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    titleText: {
        fontSize: 26,
        paddingTop: 18,
        paddingBottom: 18,
    },
    colLeft: {
        width: 100,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    colRight: {
        height: 50,
    },
    colRegion: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    textRegion: {
        color: '#04be01',
    },
    divider: {
        backgroundColor: '#CCC',
        width: '100%',
        height: 1,
    },
    phoneInput: {
        paddingLeft: 0,
    },
    rowOther: {
        marginTop: 22,
        marginBottom: 22,
    },
    textOther: {
        color: '#00506c',
    },
    buttonNext: {
        width: '100%',
        height: 50,
        marginTop: 20,
        backgroundColor: '#04be01',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textNext: {
        fontSize: 18,
    },
})


export default LoginScreen;