import React from 'react';
import { View, StyleSheet, StatusBar, TouchableOpacity, Text } from 'react-native';
import { Item, Icon, Input, Toast, List, ListItem, Left, Right, Thumbnail, Body } from 'native-base';
import { SearchUsers } from '../api/account';



class SearchScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = { search: '', result: [] };
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#ECECEC" barStyle="dark-content" />
                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                    <Item >
                        <TouchableOpacity onPress={this.onBack}><Icon name="arrow-back" /></TouchableOpacity>
                        <Input placeholder="搜索" onSubmitEditing={this.onSearch} value={this.state.value} onChangeText={(search) => this.setState({ search })} />
                        <TouchableOpacity onPress={this.onSearch}><Icon name="ios-search" /></TouchableOpacity>
                    </Item>
                </View>

                <List dataArray={this.state.result}
                    renderRow={(item) =>
                        <ListItem thumbnail onPress={() => { this.onSelect(item); }}>
                            <Left>
                                <Thumbnail square source={{ uri: item.avatar }} />
                            </Left>
                            <Body>
                                <Text>{item.nickname}</Text>
                                <Text note numberOfLines={1}>{item.note}</Text>
                            </Body>
                        </ListItem>
                    }>
                </List>
            </View>
        )
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    onSearch = (e) => {
        const { search } = this.state;
        if (!search) {
            return;
        }
        SearchUsers(search).then((body) => {
            console.log(body.data);
            this.setState({ result: body.data });
            if (!body.data || body.data.length === 0) {
                Toast.show({
                    text: '没有结果',
                    duration: 3000,
                });
            }
        }).catch(() => {
            Toast.show({
                text: '网络错误',
                duration: 4000,
            });
        });
    }

    onSelect = (u) => {
        this.props.navigation.navigate({ routeName: 'User', params: { user: u } });
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: '100%',
    },
});


export default SearchScreen;