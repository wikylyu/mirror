import React from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import { Content, Button, Icon, Text, Input, Toast } from 'native-base';
import { Col, Row } from 'react-native-easy-grid';
import { Login } from '../api/account';
import { StackActions, NavigationActions } from 'react-navigation';

class LoginNextScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: '#00000000',
            },
            headerLeft: (<Button transparent onPress={() => { navigation.goBack(); }}><Icon name='arrow-back' /></Button>),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            phone: this.props.navigation.getParam('phone', ''),
            country_code: this.props.navigation.getParam('country_code', ''),
        };
        this.loading = false;
    }
    render() {
        return (
            <Content style={styles.content}>
                <Row style={styles.row}>
                    <Text style={styles.titleText}>手机号登录</Text>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>手机号</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.text, styles.phoneInput]} placeholder='请填写手机号' value={'+' + this.state.country_code + this.state.phone} disabled></Input></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={styles.row}>
                    <Col style={styles.colLeft}><Text style={styles.text}>密码</Text></Col>
                    <Col style={styles.colRight}><Input style={[styles.text, styles.phoneInput]} placeholder='请填写密码' secureTextEntry={true} value={this.state.password} onChangeText={(password) => this.setState({ password })}></Input></Col>
                </Row>
                <View style={styles.divider}></View>
                <Row style={[styles.row, styles.rowOther]}>
                    <Text style={[styles.text, styles.textOther]}>使用短信验证码登录</Text>
                </Row>
                <Row styles={styles.row}>
                    <Button block style={styles.buttonLogin} onPress={this.onLogin}><Text style={styles.textLogin}>登录</Text></Button>
                </Row>
            </Content >
        );
    }

    onLogin = () => {
        const { phone, password } = this.state;
        if (!phone || !password || this.loading) {
            return;
        }
        this.loading = true;
        Login(phone, password).then((body) => {
            this.loading = false;
            console.log(body);
            if (body.status === 10006) {
                Toast.show({
                    text: '手机号未注册，请先注册',
                    duration: 4000,
                });
            } else if (body.status === 10007) {
                Toast.show({
                    text: '密码错误',
                    duration: 4000,
                });
            } else if (body.status !== 0) {
                throw 'Error';
            } else {
                AsyncStorage.setItem('userId', String(body.data.user_id)).then(() => { });
                AsyncStorage.setItem('token', String(body.data.id)).then(() => {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Home' })],
                    });

                    this.props.navigation.dispatch(resetAction);
                });

            }
        }).catch((err) => {
            this.loading = false;
            Toast.show({
                text: '登录失败',
                duration: 4000,
            });
        });
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    text: {
        fontSize: 14,
        color: '#333',
    },
    row: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    titleText: {
        fontSize: 26,
        paddingTop: 18,
        paddingBottom: 18,
    },
    colLeft: {
        width: 100,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    colRight: {
        height: 50,
    },
    divider: {
        backgroundColor: '#CCC',
        width: '100%',
        height: 1,
    },
    phoneInput: {
        paddingLeft: 0,
    },
    rowOther: {
        marginTop: 22,
        marginBottom: 22,
    },
    textOther: {
        color: '#00506c',
    },
    buttonLogin: {
        width: '100%',
        height: 50,
        marginTop: 20,
        backgroundColor: '#04be01',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textLogin: {
        fontSize: 18,
    },
})


export default LoginNextScreen;