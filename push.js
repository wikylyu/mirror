import PushNotification from 'react-native-push-notification';
import { AppState } from 'react-native';

PushNotification.configure({
    onNotification: function (notification) {
        console.log('NOTIFICATION: ', notification);
    },
    popInitialNotification: true,
    requestPermissions: true,
});

export const PushMessage = (id, title, message, number = '0') => {
    console.log('AppState.currentState', AppState.currentState);
    if (AppState.currentState === 'active') {
        return;
    }
    const data = {
        id: id,
        title: title,
        message: message,
        number: number,
    };
    console.log('push', data);
    PushNotification.localNotification(data);
};