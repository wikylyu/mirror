import Config from './config';


export const UploadImage = (path, type) => {
    const url = Config.Host + '/file';

    let form = new FormData()
    form.append('file', {
        uri: path,
        type: type,
        name: 'image',
    });
    return fetch(url, {
        method: 'POST',
        body: form,
    }).then((response) => response.json());
}

export const GetImageURL = (fileid) => {
    return Config.Host + '/file/' + fileid;
}