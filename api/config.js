

const Config = {
    Host: '',
    WSHost: '',
    DevHost: 'http://192.168.10.101:1212/api',
    ProdHost: 'http://light.wikylyu.xyz:1212/api',
    DevWSHost: 'ws://192.168.10.101:11111/ws',
    ProdWSHost: 'ws://light.wikylyu.xyz:11111/ws',
}

if (__DEV__) {
    Config.Host = Config.DevHost;
    Config.WSHost = Config.DevWSHost;
} else {
    Config.Host = Config.ProdHost;
    Config.WSHost = Config.ProdWSHost;
}

export default Config;